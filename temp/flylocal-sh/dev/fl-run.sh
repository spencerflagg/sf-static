echo -e "\r\n\r\n\e[44m *** Removing Code Directory *** \e[0m\r\n"
rm -r /usr/src/flylocal-web
echo -e "\r\n\r\n\e[44m *** Making Code Directory *** \e[0m\r\n"
mkdir -p /usr/src/flylocal-web
cd /usr/src
echo -e "\r\n\r\n\e[44m *** Getting Code from Gitea *** \e[0m\r\n"
git clone --depth 1 https://gitea.flylocal.us/spencer/flylocal-web.git
cd flylocal-web
curl -O https://spencerflagg.com/temp/flylocal-env/dev/.env
echo -e "\r\n\r\n\e[44m *** Killing Docker Containers *** \e[0m\r\n"
sudo docker kill $(docker ps -q)
echo -e "\r\n\r\n\e[44m *** Pruning Old Docker Images *** \e[0m\r\n"
sudo docker system prune
echo -e "\r\n\r\n\e[44m *** Building New Docker Image *** \e[0m\r\n"
sudo docker build -t flylocal-web .
echo -e "\r\n\r\n\e[44m *** Running Docker Image *** \e[0m\r\n"
sudo docker run -it -p 3000:3000 flylocal-web
