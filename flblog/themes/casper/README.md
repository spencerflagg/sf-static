# HTMLy Theme Casper
Ghost Casper theme ported to HTMLy.

## Installations 
 -  Upload and extract the zip file into themes directory.
 -  Rename the extracted folder to `casper`.
 -  Change the `views.root` using `http://www.example.com/admin/config` to `themes/casper`
 
## Dark Mode

Add new custom value in your config page.

``` 
casper.dark = "true"
```


## Custom Header

You can use custom header background for the frontpage. 

Add new custom value in your config page.

``` 
casper.header = "https://www.example.com/header.png"
```

## License

See the LICENSE.txt