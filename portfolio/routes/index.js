var express = require('express'),
    router = express.Router(),
    klawSync = require('klaw-sync');

const siteName = 'SPENCER+FLAGG';
const workPath = './public/work/Portfolio/';
const path = require('path');
const filterFn = item => {
  const basename = path.basename(item.path);
  //return basename === '.' || basename[0] !== '.';
  return !basename.includes("@");
};
const klawSyncOptions = {
  depthLimit: 0,
  filter: filterFn
};

/* GET home page. */
router.get('/', function(req, res, next) {

  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

  res.render('index', { 
    title: siteName, 
    breadcrumbs: [],
    items: klawSync(workPath,klawSyncOptions)
  });

});

router.get('/:dir', function(req, res, next) {

  res.render('index', { 
    title: siteName, 
    breadcrumbs: [
      {
        name: req.params.dir,
        path: req.params.dir
      }
    ],
    items: klawSync(workPath+ req.params.dir,klawSyncOptions)
  });

});

router.get('/:dir1/:dir2', function(req, res, next) {

  res.render('index', { 
    title: siteName, 
    breadcrumbs: [
      {
        name: req.params.dir1,
        path: req.params.dir1
      },
      {
        name: req.params.dir2,
        path: req.params.dir1 + '/' + req.params.dir2
      }
    ],
    items: klawSync(workPath+ req.params.dir1 + '/' + req.params.dir2,klawSyncOptions)
  });

});

router.get('/:dir1/:dir2/:dir3', function(req, res, next) {

  res.render('index', { 
    title: siteName, 
    breadcrumbs: [
      {
        name: req.params.dir1,
        path: req.params.dir1
      },
      {
        name: req.params.dir2,
        path: req.params.dir1 + '/' + req.params.dir2
      },
      {
        name: req.params.dir3,
        path: req.params.dir1 + '/' + req.params.dir2 + '/' + req.params.dir3
      }
    ],
    items: klawSync(workPath+ req.params.dir1 + '/' + req.params.dir2 + '/' + req.params.dir3,klawSyncOptions)
  });

});

router.get('/:dir1/:dir2/:dir3/:dir4', function(req, res, next) {

  res.render('index', { 
    title: siteName, 
    breadcrumbs: [
      {
        name: req.params.dir1,
        path: req.params.dir1
      },
      {
        name: req.params.dir2,
        path: req.params.dir1 + '/' + req.params.dir2
      },
      {
        name: req.params.dir3,
        path: req.params.dir1 + '/' + req.params.dir2 + '/' + req.params.dir3
      },
      {
        name: req.params.dir4,
        path: req.params.dir1 + '/' + req.params.dir2 + '/' + req.params.dir3 + '/' + req.params.dir4
      }
    ],
    items: klawSync(workPath+ req.params.dir1 + '/' + req.params.dir2 + '/' + req.params.dir3 + '/' + req.params.dir4,klawSyncOptions)
  });

});


module.exports = router;