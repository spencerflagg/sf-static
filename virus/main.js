let metaElement = document.getElementById('meta');
let dbElement = document.getElementById('db');

let addUrl = document.getElementById('addUrl');
let addButton = document.getElementById('addButton');
let addComment = document.getElementById('addComment');
let addTags = document.getElementById('addTags');

const herokuUrl = '';//'https://cors-anywhere.herokuapp.com/'

const endpointUrl = 'https://virus-vs-state-endpoint.azurewebsites.net/articles/';

addButton.onclick = function() {add()};

$.get(herokuUrl + endpointUrl, function(articles) {

    var list = document.createElement("UL");
    console.log(articles);
    
    articles.forEach(element => {
        var item = document.createElement("LI");
        var title = document.createElement("H1");
        var link = document.createElement("A");
        var desc = document.createElement("P");
        var img = document.createElement("DIV");

        title.innerHTML = element.Title;
        desc.innerHTML = element.Description;

        //GET IMAGE
        // $.get(herokuUrl + element.Url, function(data) {
        //     var $data = $(data);
        //     var imageUrl = 
        //         $data.filter('meta[property="og:image"]').attr("content")||"";
        //     //img.setAttribute('src',imageUrl);
        //     img.setAttribute('id','img');
        //     img.style.backgroundImage = "url(" + imageUrl + ")";
        
        // });

        
        link.setAttribute('href',element.Url);
        link.appendChild(title);
        link.appendChild(desc);
        link.appendChild(img);

        item.appendChild(link);
        list.appendChild(item);
    });
    dbElement.appendChild(list);
            
});

function add(){

    let p = new Promise((resolve, reject) => {

        $.get(herokuUrl + addUrl.value, function(data) {
            var $data = $(data);
            var title = $data.filter('meta[name=title]').attr("content")||
                $data.filter('meta[property="og:title"]').attr("content")||
                $data.filter('meta[property="twitter:title"]').attr("content");
            var description = $data.filter('meta[name=description]').attr("content")||
                $data.filter('meta[property="og:description"]').attr("content")||
                $data.filter('meta[property="twitter:description"]').attr("content");
            resolve({title:title, description:description});
        });

    });

    p.then((data) => {

        axios.post(herokuUrl + endpointUrl, {
            // Title: data.title, 
            // Description: data.description,
            // Url: addUrl.value,
            // Time: Date.now(),
            // UserId: 0,
            // Comment: addComment.innerText,
            // Tags: addTags.innerText,

            // 'Title': data.title, 
            // 'Description': data.description,
            // 'Url': addUrl.value,
            // 'Time': Date.now(),
            // 'UserId': 0,
            // 'Comment': addComment.innerText,
            // 'Tags': addTags.innerText,

            'Title': "asdf", 
            'Description': "asdf",
            'Url': "asdf",
            'Time': "2001-01-01 00:00",
            'UserId': "0",
            'Comment': "asdf",
            'Tags': "asdf",
          }, {
            headers: {
            'Content-Type': 'application/json',
            //'Origin': 'xyz'
            'X-Requested-With': 'XMLHttpRequest'
            }
          })
          .then(function (response) {
            console.log(response);
            $("pre").html(response);
          })
          .catch(function (error) {
            console.log(error);
            $("pre").html(error);
          });
        

    });

}