const fs = require('fs');

// Papa.parsePromise = function(file) {
//     return new Promise(function(complete, error) {
//       Papa.parse(file, {complete, error});
//     });
//   };

function getCsv(){

    return fetch('db.csv')
        .then(response => response.text())
        .then(text => {
            
            const config ={
                header: true
            };
            var result =Papa.parse(text, config);
            console.group('csv');
            console.log(result.data[0]);
            console.groupEnd('csv');
            return result.data;
            // (returns Promise)

        });

}


function renderHello() {

    getCsv().then((csvResult)=>{

        var categorizedList = [];

        var sortedList = csvResult.sort(function(a,b){

            return b.followers - a.followers;

        });

        sortedList.forEach((row) => {
            
            if(row.name == '')
            {
                return;
            }


            row.category.split(',').forEach((thisCategory) => {

                thisCategory = thisCategory.trim();
                var existingCategory = categorizedList.filter(e => e.category === thisCategory);

                if(existingCategory.length == 0){
                    var newCategory = {};
                    newCategory.category = thisCategory;
                    var newItemsArray = [];
                    newItemsArray.push(row);
                    newCategory.items = newItemsArray;
                    categorizedList.push(newCategory);
                    console.group('new');
                    console.log(newCategory);
                    console.groupEnd('new');
                }else{
                    existingCategory[0].items.push(row);
                    console.group('exist');
                    console.log(existingCategory);
                    console.groupEnd('exist');
                }

            });
            
        });

        console.log(categorizedList);

        fetch('item.mustache.txt')
            .then((response) => response.text())
            .then((template) => {
                var rendered = Mustache.render(template, { categories: categorizedList } );
                document.getElementById('target').innerHTML = rendered;
            });
    });

  }