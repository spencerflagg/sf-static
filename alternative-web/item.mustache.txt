<header>
    <p>THE</p>
    <p class='alternative'>ALTER<span class='dot'>•</span></p>
    <p class='alternative'>NATIVE</p>
    <p>WEB</p>
</header>
<nav class='contact'>
    <ul>
        <li><span class='suggestions'><img class='arrow' src='https://openmoji.org/data/black/svg/2B05.svg'/>make a<br> suggestion</span></li>
        <li><a href="mailto:team@wearvol.com"><img src="https://openmoji.org/data/black/svg/2709.svg" /><span>email</span></a></li>
        <li><a target="_blank" href="https://twitter.com/wearvol"><img src="https://openmoji.org/data/black/svg/1F426.svg" /><span>twitter</span></a></li>
        <li><a target="_blank" href="https://www.instagram.com/wearvol/"><img src="https://openmoji.org/data/black/svg/1F4F7.svg" /><span>instagram</span></a></li>
        <li><a target="_blank" href="https://gab.com/wearvol"><img src="https://openmoji.org/data/black/svg/1F438.svg" /><span>gab</span></a></li>
        <li><a target="_blank" href="https://www.minds.com/wearvol/"><img src="https://openmoji.org/data/black/svg/1F4A1.svg" /><span>minds</span></a></li>
        <li><a target="_blank" href="https://steemit.com/@wearvol"><img src="https://openmoji.org/data/black/svg/1F682.svg" /><span>steemit</span></a></li>
        <li><a target="_blank" href="https://peepeth.com/wearvol"><img src="https://openmoji.org/data/black/svg/1F427.svg" /><span>peepeth</span></a></li>
        <li><a target="_blank" href="https://liberdon.com/@wearvol"><img src="https://openmoji.org/data/black/svg/1F5FD.svg" /><span>liberdon</span></a></li>
        <li><a target="_blank" href="https://twetch.app/u/4869"><img src="https://openmoji.org/data/black/svg/E245.svg" /><span>twetch</span></a></li>
        <li><a target="_blank" href="https://flote.app/WearVol"><img src="https://openmoji.org/data/black/svg/26F5.svg" /><span>flote</span></a></li>
    </ul>
</nav>
<main>
    {{#categories}}
        <section>
            <h1 id='{{& category}}'>{{category}}</h1>
            <ul class='items'>
                {{#items}}
                    <li class='item'> 
                        <div class='item-title'>
                            <h2>
                                <a href='{{& url }}' target='_blank'>{{ name }}
                                    {{#imageUrl}}<div class='icon-app' style="background-image: url('{{& imageUrl }}')"></div>{{/imageUrl}}
                                </a>
                            </h2>
                            {{#twitter}}
                                <a class='link-twitter' href='http://twitter.com/{{& twitter }}' target='_blank'>@{{ name }}<img class='icon-twitter' style="background-image: url('https://www.openmoji.org/data/color/svg/E040.svg')"/></a>
                            {{/twitter}}
                        </div>
                        <ul class='badges'>
                            {{#dx}}
                                <li onclick="" class='badge-dx badge-{{& . }}'>
                                    <div class='flyout'>
                                        <h3><span>decentralized</span></h3>
                                        <p>Content is not stored in a central location.</p>
                                        </div>
                                </li>
                            {{/dx}}
                            {{#bc}}
                                <li onclick="" class='badge-bc badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>blockchain</span></h3>
                                    <p>Content is stored in a decentralized ledger. <a href='https://everipedia.org/wiki/lang_en/blockchain-3' target='_blank'>(MORE)</a></p>
                                    </div>
                                </li>
                            {{/bc}}
                            {{#fd}}
                                <li onclick="" class='badge-fd badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>federated</span></h3>
                                    <p>Content is stored on independently hosted, interconnected servers. <a href='https://everipedia.org/wiki/lang_en/Fediverse' target='_blank'>(MORE)</a></p>
                                    </div>
                                </li>
                            {{/fd}}
                            {{#an}}
                                <li onclick="" class='badge-an badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>anonymous</span></h3>
                                    <p>Can be used without sharing identifying information.</p>
                                    </div>
                                </li>
                            {{/an}}
                            {{#en}}
                                <li onclick="" class='badge-en badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>encrypted</span></h3>
                                    <p>Protects your information from 3rd parties. <a href='https://everipedia.org/wiki/lang_en/Encryption' target='_blank'>(MORE)</a></p>
                                    </div>
                                </li>
                            {{/en}}
                            {{#pr}}
                                <li onclick="" class='badge-pr badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>protocol</span></h3>
                                    <p>Relies on networks and structures outside of the mainstream internet. <a href='https://everipedia.org/wiki/lang_en/Communications_protocol' target='_blank'>(MORE)</a></p>
                                    </div>
                                </li>
                            {{/pr}}
                            {{#os}}
                                <li onclick="" class='badge-os badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>open source</span></h3>
                                    <p>Developed and maintained by a decentralized community. <a href='https://everipedia.org/wiki/lang_en/Open-source_software' target='_blank'>(MORE)</a></p>
                                    </div>
                                </li>
                            {{/os}}
                            {{#fs}}
                                <li onclick="" class='badge-fs badge-{{& . }}'>
                                    <div class='flyout'>
                                    <h3><span>free speech</span></h3>
                                    <p>Focused on free speech and anti-censorship.</p>
                                    </div>
                                </li>
                            {{/fs}}
                        </ul>
                        <p class='desc'>{{description}}</p>
                    </li>
                {{/items}}
            </ul>
        </section>
    {{/categories}}
</main>
<footer>
    <nav class='glossary'>
        <ul>
            <li><span class='todo'>TODO</span> Replace Your Old Web:</li>
            {{#categories}}
                <li>
                    <a href='#{{& category}}'>{{category}}</a>
                </li>
            {{/categories}}
        </ul>
    </nav>
    <div class='vol'>A&nbsp;<a href='https://wearvol.com'>VOL ATTIRE</a>&nbsp;PROJECT</div>
</footer>
